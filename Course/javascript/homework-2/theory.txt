1) Main role of functions in any programming language is combining similar groups of actions. 
Programmers create functions in order not to repeat the same part of code multiple times.

2) Arguments in JavaScript are used in order to pass different data in our functions. 
If arguments are not defined, their values become "undefined".