
function createNewUser() {

    this.firstName = prompt("Enter your name: ");
    while (this.firstName === ""){
        this.firstName = prompt("Enter your name again: ");
    }

    this.lastName = prompt('Enter your second name','');
    while (this.lastName === ''){
        this.lastName = prompt('Enter your second name again: ','');
    }

    this.getLogin = function(){
        let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    }
}

let newUserObj = new createNewUser();
alert(`Your login is: ${newUserObj.getLogin()}`);
