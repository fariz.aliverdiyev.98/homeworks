document.getElementById("demo").innerHTML="Hello, this is my list:";

let select = document.querySelector("select");
let par = document.querySelector("p2");

select.addEventListener("change",setLanguage);

function setLanguage(){
    let choice = select.value;

    if(choice=="python"){
        par.textContent = "Python is the best language for beginners in programming.";
    }else if(choice=="javascript"){
        par.textContent = "Javascript is the most widely used language nowadays.";
    }else if(choice=="golang"){
        par.textContent = "Go language has good perspctives in the field of technologies.";
    }else if(choice=="scala"){
        par.textContent = "Scala is the functional PL that is why it will be very difficult for beginners to start with it."
    }else{
        par.textContent = "";
    }
}