var op; //выбранный оператор
  function func() {
    var result;
    var num1 = Number(document.getElementById("first__num").value);
    var num2 = Number(document.getElementById("second__num").value);
    switch (op) {
      case '+':
        result = num1 + num2;
        break;
      case '-':
        result = num1 - num2;
        break;
      case '*':
        result = num1 * num2;
        break;
      case '/':
        if (num2) {
          result = num1 / num2;
        } else {
          result = 'infinity';
        }
        break;
      default:
        result = 'select operation';
    }

    document.getElementById("result").innerHTML = result;
  }